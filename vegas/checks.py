"""This file provides multiple helper `checks` that make dealing with discord.py easier."""
import functools

import discord
from discord.ext import commands

def has_user_ping():
    async def predicate(ctx):
        if ctx.message.mentions:
            return True
        await ctx.send("You have to provide a user ping!")
        return False
    return discord.ext.commands.check(predicate)

