"""This file provides multiple helper Python functions"""
import datetime
import asyncio
import typing
from operator import attrgetter


def asnyc_to_sync(coro: typing.Coroutine) -> typing.Any:
    """Allows async functions to run in non-async functions"""
    task = asyncio.create_task(coro)
    asyncio.get_running_loop().run_until_complete(task)
    return task.result()

#given a datetime object returns a tuple of how long ago that object happened, if it's over a week, over a month, over a year ago
def to_relative_delta(given_time: datetime.datetime):
    now = datetime.datetime.now()
    delta = now - given_time
    seconds = abs(int(delta.seconds))
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    #sets additional return parameters
    week = month = year = new = False
    if days == 0 and hours == 0:
        new = True
    elif days > 7:
        week = True
    elif days > 31:
        month = True
    elif days > 365:
        year = True
    #returns appropriate output string and bools for special Account age cases
    if days > 0:
        return '{} day(s) ago'.format(days), new, week, month, year
    elif hours > 0:
        return '{} hour(s) and {} minute(s) ago'.format(hours, minutes), new, week, month, year
    elif minutes > 0:
        return '{} minute(s) ago'.format(minutes), new, week, month, year
    elif seconds > 0:
        return '{} second(s) ago'.format(seconds), new, week, month, year
    else:
        return "bruh idek;\n now: {}\ngiven: {}\ndelta: {}".format(now, given_time, delta)

